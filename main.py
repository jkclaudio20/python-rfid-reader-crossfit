#!/usr/bin/env python

import sys
import glob
import serial
import io
import threading
import MySQLdb
import datetime
from guizero import *

class Ports:
    def serialPorts():
        if sys.platform.startswith('win'):
            ports = ['COM%s' % (i + 1) for i in range(256)]
        elif sys.platform.startswith('linux') or sys.platform.startswith('cygwin'):
            ports = glob.glob('/dev/tty[A-Za-z]*')
        elif sys.platform.startswith('darwin'):
            ports = glob.glob('/dev/tty.*')
        else:
            raise EnvironmentError('Unsupported Platform')

        result = []
        for port in ports:
            try:
                s = serial.Serial(port)
                s.close()
                result.append(port)
            except (OSError, serial.SerialException):
                pass

        return result
    
class Reader(threading.Thread):
    def __init__(self, port, baudrate=9600):
        print('Reader initialized!')
        self.connect(port, baudrate)
        
    def connect(self, port, baudrate):
        self.ser = serial.Serial(port, baudrate)
        self.readData()
        
    def readData(self):
        while True:
            print(self.ser.readline())
            Gui.self.displayData(self.ser.readline())

class Gui:
    def __init__(self):
        self.app = App(title="RFID Connection Setup", layout="grid", width=400, height=200)
        self.selectedPort = ""
        self.selectedHost = ""
        self.createLayout()
        self.run()

    def createLayout(self):
        self.topBox = Box(self.app, layout="grid", grid=[1,1], height=30, width=1)
        self.leftBox = Box(self.app, layout="grid", grid=[1,2], width=30, height=1)
        self.mainBox = Box(self.app, layout="grid", grid=[2,2], align="left")

        self.connectionBox = Box(self.mainBox, layout="grid", grid=[1,2], align="left")

        self.portLabel = Text(self.connectionBox, text="Port:", grid=[1,2], align="left")
        self.availablePorts = Ports.serialPorts()
        self.port = Combo(self.connectionBox, options=self.availablePorts, grid=[2,2], width=20, align="left")

        self.hostLabel = Text(self.connectionBox, text="IP Address:", grid=[1,3], align="left")
        self.host = TextBox(self.connectionBox, width=25, grid=[2,3], align="left", text="111.221.46.246")

        self.connectButton = PushButton(self.connectionBox, text="Connect", grid=[2,4], align="left", command=self.mainUi)

    def run(self):
        self.app.display()

    def mainUi(self):
        self.app.hide()
        
        self.serialPort = self.port.get()
        self.thread = threading.Thread(target=self.connect, args=(self.serialPort,))
        self.thread.start()
        
        # Connect to DB
        self.db = MySQLdb.connect(host=self.host.value,
                                  user="posit_posuser",
                                  passwd="#2)cfh{[DMf0",
                                  db="posit_crossfit")
        
        self.db.autocommit(True)
        
        
        self.window = Window(self.app, layout="grid", title="RFID Connected: ("+self.serialPort+" on " + self.host.get() +")", width=1000, height=600)
        self.window.on_close(self.exitApp)
        
        self.displayTopBox = Box(self.window, grid=[1,1], width=1, height=80)
        self.displayLeftBox = Box(self.window, grid=[1,2], width=80, height=1)
        self.displayCenterBox = Box(self.window, layout="grid", grid=[2,2], width=20, height=1)
        self.tapText = Text(self.displayCenterBox, text="Tap your Card!", grid=[1,1], size=30)
        
        self.separatorBox = Box(self.window, layout="grid", grid=[2,3], width=1, height=50)
        self.responseBox = Box(self.window, layout="grid", grid=[2,4], width=200, height=200)
        
        self.responseData = Text(self.responseBox, text="", grid=[1,1])
        
        self.displayCenterBox
        
    def connect(self, port, baudrate=9600):
        self.ser = serial.Serial(port, baudrate)
        self.readData()
        
    def readData(self):
        while True:
            self.displayData(self.ser.readline())
        
    def displayData(self, rfid_tag):
        self.responseData.value = "Reading card..."
        
        rfid_tag = rfid_tag[:-4]
        rfid_no  = rfid_tag.decode('utf-8')
        
        dbCursor = self.db.cursor()
        dbCursor.execute("SELECT * FROM sma_members WHERE rfid_no = '"+rfid_no+"' LIMIT 1")
        row = dbCursor.fetchone()
        
        
        if (row):
            now = datetime.datetime.now()
            dateNow = now.strftime('%Y-%m-%d')
            dateTimeNow = now.strftime('%Y-%m-%d %H:%M:%S')
            displayNow = now.strftime('%B %-d, %Y %-I:%M %p')
            
            # check existing attendance
            dbCursor.execute("SELECT * FROM sma_rfid_taps WHERE rfid_no = '"+rfid_no+"' AND date LIKE '"+dateNow+"%' ORDER BY id DESC LIMIT 1")
            
            lastTapRow = dbCursor.fetchone()
            
            if (lastTapRow):
                if (lastTapRow[3] == 'in'):
                    tap_type = 'out'
                    dbCursor.execute("INSERT INTO sma_rfid_taps (rfid_no, date, type) VALUE ('"+rfid_no+"','"+dateTimeNow+"', 'out')")
                else:
                    tap_type = 'in'
                    dbCursor.execute("INSERT INTO sma_rfid_taps (rfid_no, date, type) VALUE ('"+rfid_no+"','"+dateTimeNow+"', 'in')")
            else:
                tap_type = 'in'
                dbCursor.execute("INSERT INTO sma_rfid_taps (rfid_no, date, type) VALUE ('"+rfid_no+"','"+dateTimeNow+"', 'in')")
                    
            
            self.responseData.value = ""
            self.responseData.value = row[2] + " " + row[4] + " | Tapped " + tap_type + " at " + displayNow
        else:
            self.responseData.value = ""
            self.responseData.value = "Unknown card!"     

    def exitApp(self):
        self.app.destroy()

Gui()
